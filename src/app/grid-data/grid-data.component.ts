import { Component, OnInit, Input  } from '@angular/core';
import {ClientService} from '../client.service';
import {ApiService} from '../api.service';


@Component({
  selector: 'app-grid-data',
  templateUrl: './grid-data.component.html',
  styleUrls: ['./grid-data.component.css']
})
export class GridDataComponent implements OnInit {

  columnDefs = [
    {headerName: 'BookingId', field: 'bookingid', sortable: true, sortingOrder: ['asc','desc'], filter: "agNumberColumnFilter"},
    {headerName: 'Cliente', field: 'cliente', filter: true},
    {headerName: 'Fecha de Creación', field: 'fechadecreacion', sortable: true, sortingOrder: ['asc','desc'], filter:true},
    {headerName: 'Dirección', field: 'direccion', filter:true},
    {headerName: 'Precio', field: 'precio' , filter: "agNumberColumnFilter"}
  ];

	rowData = [];
	
  testData = [];
  
  gridOptions = {
    //this options translate datagrid to spanish
    localeText : {
      // for filter panel
      page: 'pagina',
      more: 'mas',
      to: 'a',
      of: 'de',
      next: 'siguiente',
      last: 'ultimo',
      first: 'primera',
      previous: 'previa',
      loadingOoo: 'cargando...',

      // for set filter
      selectAll: 'seleccionar todo',
      searchOoo: 'buscando...',
      blanks: 'blancos',

      // for number filter and text filter
      filterOoo: 'filter...',
      applyFilter: 'Aplicar Filtro...',
      equals: 'igual',
      notEqual: 'no igual',

      // for number filter
      lessThan: 'menor que',
      greaterThan: 'mas que',
      lessThanOrEqual: 'menor o igual',
      greaterThanOrEqual: 'mayor o igual',
      inRange: 'en rango',

      // for text filter
      contains: 'contiene',
      notContains: 'no contiene',
      startsWith: 'comienza con',
      endsWith: 'termina con',

      // the header of the default group column
      group: 'grupo',

      // tool panel
      columns: 'Columnas',
      filters: 'Filtros',
      rowGroupColumns: 'Grupo de filas',
      rowGroupColumnsEmptyMessage: 'vacio',
      valueColumns: 'valor columna',
      pivotMode: 'modo pivote',
      groups: 'grupos',
      values: 'volres',
      pivots: 'pivotes',
      valueColumnsEmptyMessage: 'columnas a agregar',
      pivotColumnsEmptyMessage: 'pivotes que agregar',
      toolPanelButton: 'panel',

      // other
      noRowsToShow: 'No hay filas',

      // enterprise menu
      pinColumn: 'pin columna',
      valueAggregation: 'agregar valor',
      autosizeThiscolumn: 'tamaño automatico esta columna',
      autosizeAllColumns: 'tamaño automatico todas las columnas',
      groupBy: 'agrupado por',
      ungroupBy: 'no agrupado por',
      resetColumns: 'limpiar columnas',
      expandAll: 'expandir',
      collapseAll: 'colapsar',
      toolPanel: 'barra de herramientas',
      export: 'exportar',
      csvExport: 'exportar csv',
      excelExport: 'exportar excel',

      // enterprise menu pinning
      pinLeft: ' <<',
      pinRight: ' >>',
      noPin: '<>',

      // enterprise menu aggregation and status bar
      sum: 'sum',
      min: 'min',
      max: 'max',
      none: 'ninguno',
      count: 'contador',
      average: 'promedio',

      // standard menu
      copy: 'copiar',
      copyWithHeaders: 'copiar con encabezado',
      ctrlC: 'ctrl + C',
      paste: 'pegar',
      ctrlV: 'ctrl + V'
    }

  }

  constructor(private client: ClientService, private api: ApiService) { 
  }

  ngOnInit() {

   if(this.client.isLogged){
    const token = this.client.getToken();
    console.log(token);

    this.api.getData(
      this.client.getAdmin(),
      'contacto@tuten.cl',
      this.client.getToken()
    )
      .subscribe(
        r => {
          for(var obj in r)
          {
            // transform bookingFields to get client Name
            
            var client = JSON.parse(r[obj].bookingFields);
            // parse date to display dd/mm/yyyy format
            var dateTime = r[obj].bookingTime;

            var date = new Date(parseInt(dateTime));
            var options = { year: 'numeric', month: '2-digit', day: '2-digit' };
            dateTime = date.toLocaleDateString('es-005',options);
      
            // put data in temp array
            this.testData.push({bookingid: r[obj].bookingId, cliente: client.firstName+' '+client.lastName , fechadecreacion: dateTime, direccion: client.address , precio: r[obj].bookingPrice });
		
          }
			  // put data into array to show in grid
			  this.rowData =  this.testData;

        },
        r => {
          alert(r.error.error);
        });
   }

  }

}
