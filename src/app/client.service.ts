import {Injectable} from '@angular/core';

const TOKEN = 'TOKEN';
const ADMIN = '';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  setToken(token: string): void {
    localStorage.setItem(TOKEN, token);
  }

  setAdmin(admin: string): void {
    localStorage.setItem(ADMIN, admin);
  }

  getAdmin(){
    return localStorage.getItem(ADMIN);
}

  isLogged() {
    return localStorage.getItem(TOKEN) != null;
  }

  getToken(){
      return localStorage.getItem(TOKEN);
  }
}