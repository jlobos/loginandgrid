import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';
import {Observable, of } from 'rxjs';
import {map, catchError } from 'rxjs/operators';


@Injectable({
    providedIn: 'root'
  })
export class ApiService {

  constructor(private http: HttpClient) {

  }

  /**
   * login 
   * @param usuario : user to login 
   * @param password : password
   * 
   * @returns data from api
   */
  login(usuario: string, password: string) : Observable<any>{
    const endpoint = 'https://dev.tuten.cl:443/TutenREST/rest/user/';
    const httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json', 'password': password , 'app': 'APP_BCK' })
      };

      var r = this.http.put<any>(endpoint+usuario, null, httpOptions).pipe(
        map(this.extractData),
        catchError(this.handleError<any>('login'))
      );
      return this.http.put<any>(endpoint+usuario, null, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError<any>('login'))
    );
  }
  /**
   * getData
   * @param admin: admin user 
   * @param usuario: user to query
   * @param token: token get from login form
   */

  getData(admin: string, usuario: string, token: string) : Observable<any>{
    const httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json', 'adminemail': admin, 'token': token , 'app': 'APP_BCK' })
      };
      const url = 'https://dev.tuten.cl:443/TutenREST/rest/user/'+usuario+'/bookings?current=true';
    const myObj ={
        adminemail: admin, 
        token: token , 
        app: 'APP_BCK'
      };
    return this.http.get<any>(url, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError<any>('getData'))
    );
  }


  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  /**
   * handleError
   * @param operation : name of operation
   * @param result : result
   * display errors
   */

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      //show error to user
      alert(operation + ' falló '+ error.message);
  
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} falló: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}