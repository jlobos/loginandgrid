import { Component, Input, NgZone, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ApiService} from '../api.service';
import {ClientService} from '../client.service';
import {FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;


  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      usuario: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(3)]]
  });
  }
  
 

 
  
  //@Input() usuario: string; //= 'testapis%40tuten.cl';
  //@Input() password: string; //= '1234';

  constructor(private formBuilder: FormBuilder, private api: ApiService, private client: ClientService, private router: Router, private zone:NgZone) {
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.registerForm.invalid) {
          return;
      }

      this.login();
  }


  login() {

    this.api.login(
      this.registerForm.get('usuario').value,
      this.registerForm.get('password').value
    )
      .subscribe(
        r => {
          if(r){
            if (r.sessionTokenBck) {
              console.log(r.sessionTokenBck);
              console.log(r.email);
              this.client.setToken(r.sessionTokenBck);
              this.client.setAdmin(r.email);
              this.router.navigateByUrl('/grid');
            }
            else{
              alert(r.error.error);
            }
        }
        },
        r => {
          alert(r.error.error);
        });
  }

}
